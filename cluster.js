const cluster = require('cluster');
const http = require('http');
const numCPUs = require('os').cpus().length/2;

if (cluster.isMaster) {

  console.log(`Master ${process.pid} is running`);

  // Fork workers.
  for (let i = 0; i < numCPUs; i++) {
    cluster.fork();
  }

  // Restart worker on crash.
  cluster.on('exit', (worker, code, signal) => {
    console.log(`worker ${worker.process.pid} died ...restarting`);
    cluster.fork();
  });

} else {

  // Start the server.
  const child = require('./server.js');
  console.log(`Worker ${process.pid} started`);

}
