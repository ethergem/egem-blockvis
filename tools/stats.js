"use strict"

const Web3 = require("web3");
var net = require('net');
var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/";
var dbSettings = { useUnifiedTopology: true };
const settings = require("../cfgs/settings.json");
//var web3 = new Web3(new Web3.providers.IpcProvider(settings.web3IPC, net));
var web3 = new Web3(new Web3.providers.HttpProvider(settings.web3HTTP));
const fetch = require('node-fetch');

MongoClient.connect(url,dbSettings, function(err, db) {
  if (err) throw err;

  // set database
  var dbo = db.db("egemEXP");

  function countBlocks() {
    dbo.collection("blocks").countDocuments(function(err, res) {
      if (err) throw err;
      console.log(res);
      var myquery = { blocks: { $gt: -1 } };
      var newvalues = { $set: {blocks: res} };
      dbo.collection("information").updateOne(myquery, newvalues, function(err, res) {
        if (err) throw err;
        console.log("Blocks updated");
      });
    });
  }

  function countUncles() {
    dbo.collection("uncles").countDocuments(function(err, res) {
      if (err) throw err;
      console.log(res);
      var myquery = { uncles: { $gt: -1 } };
      var newvalues = { $set: {uncles: res} };
      dbo.collection("information").updateOne(myquery, newvalues, function(err, res) {
        if (err) throw err;
        console.log("Uncles updated");
      });
    });
  }

  function countTxs() {
    dbo.collection("transactions").countDocuments(function(err, res) {
      if (err) throw err;
      console.log(res);
      var myquery = { transactions: { $gt: -1 } };
      var newvalues = { $set: {transactions: res} };
      dbo.collection("information").updateOne(myquery, newvalues, function(err, res) {
        if (err) throw err;
        console.log("Transactions updated");
      });
    });
  }

  function countAccounts() {
    dbo.collection("accounts").countDocuments(function(err, res) {
      if (err) throw err;
      console.log(res);
      var myquery = { accounts: { $gt: -1 } };
      var newvalues = { $set: {accounts: res} };
      dbo.collection("information").updateOne(myquery, newvalues, function(err, res) {
        if (err) throw err;
        console.log("Accounts updated");
      });
    });
  }

  function populateRichlist() {
    dbo.collection("accounts").find().toArray(function(err, result) {
      if (err) throw err;
      //console.log(result);
        let richlist = result;

        function delay() {
          return new Promise(resolve => setTimeout(resolve, 50));
        }

        async function delayedLog(item) {
          // await promise return
          await delay();
          var getbal = await web3.eth.getBalance(item.address);
          let i = item.address;

          let query = { address: i };
          //console.log(query);
          dbo.collection("accounts").find(query).toArray(function(err, res) {
            if (err) throw err;
            if (res.length > 0) {
              var finbal = Number(getbal/Math.pow(10,18));

              var newvalues = { $set: {bal: Math.round(finbal)} };
              //console.log(newvalues);
              dbo.collection("accounts").updateOne(query, newvalues, function(err, res) {
                if (err) throw err;
                //console.log("User sent updated");
              });
            }
          });
        }

        async function processArray(array) {
          console.log("Richlist update started.");
          for (const item of array) {
            await delayedLog(item);
          }
          console.log("Richlist updated.");
        }
        processArray(richlist);
    })
  }

  function tallySupply() {
    dbo.collection("accounts").find().toArray(function(err, result) {
      if (err) throw err;
      //console.log(result);
        let richlist = result;
        let start = 0;

        function delay() {
          return new Promise(resolve => setTimeout(resolve, 1));
        }

        async function delayedLog(item) {
          // await promise return
          await delay();
          start += item.bal;
          //console.log(item.bal);
        }

        async function processArray(array) {
          console.log("Supply tally started.");
          for (const item of array) {
            await delayedLog(item);
          }

          var myquery = { supply: { $gt: -1 } };
          var newvalues = { $set: {supply: start} };
          dbo.collection("information").updateOne(myquery, newvalues, function(err, res) {
            if (err) throw err;
            console.log("Supply tally updated: "+ start);
          });
        }

        processArray(richlist);
    })
  }

  function getPrice() {
    let url = "https://api.coingecko.com/api/v3/simple/price?ids=ethergem&vs_currencies=USD";

    let settings = { method: "Get" };

    fetch(url, settings)
    .then(res => res.json())
    .then((json) => {
      var value = json['ethergem']['usd'];
      var myquery = { price: { $gt: -1 } };
      var newvalues = { $set: {price: value} };
      dbo.collection("information").updateOne(myquery, newvalues, function(err, res) {
        if (err) throw err;
        console.log("New price stored.");
      });
    });
  }

  function sumContracts() {
    dbo.collection("accounts").find().toArray(function(err, result) {
      if (err) throw err;
      //console.log(result);
        let richlist = result;
        let start = 0;

        function delay() {
          return new Promise(resolve => setTimeout(resolve, 1));
        }

        async function delayedLog(item) {
          // await promise return
          await delay();
          start += item.contracts;
          //console.log(item.bal);
        }

        async function processArray(array) {
          console.log("Contract tally started.");
          for (const item of array) {
            await delayedLog(item);
          }
          var myquery = { contracts: { $gt: -1 } };
          var newvalues = { $set: {contracts: start} };
          dbo.collection("information").updateOne(myquery, newvalues, function(err, res) {
            if (err) throw err;
            console.log("Contract tally updated: "+ start);
          });
        }

        processArray(richlist);
    })
  }

  console.log("Stats updating started.")

  setInterval(async function() {
    await countBlocks();
    await countUncles();
    await countTxs();
    await countAccounts();
    await getPrice();
  }, 30000)

  setInterval(async function() {
    await populateRichlist();
    await tallySupply();
    await sumContracts();
  }, 1200000)
  
});
