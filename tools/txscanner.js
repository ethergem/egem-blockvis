"use strict"

const Web3 = require("web3");
const InputDataDecoder = require('ethereum-input-data-decoder');
const abiDecoder = require('abi-decoder');
var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/";
var dbSettings = { useUnifiedTopology: true };
const settings = require("../cfgs/settings.json");
var web3 = new Web3(new Web3.providers.HttpProvider(settings.web3HTTP));

var speed = 25; // ms
var start = 12614;
var page = 1;
var limitPer = 1000;

function getTxs() {
  MongoClient.connect(url,dbSettings, function(err, db) {
    var dbo = db.db("egemEXP");
    if (err) throw err;

    // calculate offset
    const offset = (page - 1) * limitPer

    dbo.collection("transactions").find( { blocknum: { $gt: start } } ).skip(Number(offset)).limit(Number(limitPer)).sort({ blocknum: 1}).toArray(function(err, result) {
      if (err) throw err;

      function delay() {
        return new Promise(resolve => setTimeout(resolve, speed));
      }

      async function delayedLog(item) {
        // await promise return
        await delay();

        if (item.inputData.length > 2) {
          try {
            web3.eth.getTransactionReceipt(item.hash, function(err, txResult){
              if (!err) {
                if (txResult) {
                  console.log(txResult.contractAddress);

                  let contractAddress = item.toUser;
                  let createdAddress = txResult.contractAddress;
                  // insert user
                  function insertUser(i) {
                    var query = { address: i }
                    let data = {
                      address: i,
                      type: 1,
                      contracts: 0,
                      sent: 0,
                      recv: 0,
                      minedblocks: 0,
                      mineduncles: 0,
                      bal: 0
                    }
                    console.log(data);
                    dbo.collection("accounts").insertOne(data, function(err, res) {
                      if (err) console.log("Address Skipped.");
                      //console.log("1 user inserted");
                    });
                  }
                  if (createdAddress) {
                    insertUser(createdAddress);
                  }
                  try {
                    let abi = require(`${__dirname}/abi/`+contractAddress+`.json`);
                    console.log(item.input);
                    //console.log(contractAddress);
                    //console.log(txResult.input);
                    console.log("-------");

                    abiDecoder.addABI(abi);

                    let decoded = abiDecoder.decodeMethod(item.inputData);
                    console.log(decoded);
                  } catch (e) {
                    console.log(e);
                  }

                }
              }
            })

          } catch (e) {
            console.log(e);
            console.log("Contract ABI not found, not converting input data.");
          }
        } else {
          console.log(item.inputData);
          console.log("do normal tx things " + item.blocknum);
        }
      }

      async function processArray(array) {
        console.log("Current Chunk: "+page);
        for (const item of array) {
          await delayedLog(item);
        }
        page++
        getTxs();
      }

      processArray(result);

    })
  })
}
getTxs()
//console.log(`${__dirname}`);
//0x900029014e1042cfacedce7b638d1b119717cc83b053e92552c150e48f5afa0b
//0x9a3a15391e1b277e5b6358c37b3ef348bbd02946cc68b761b5bd3f04438bf306
// let txhash = "0x551e258e027583691c77e0edeb04db232cb5102a109473e6286b81b534a8f566";
//
// web3.eth.getTransaction(txhash, (error, txResult) => {
//
//   if (txResult.input.length > 2) {
//     try {
//       let contractAddress = web3.utils.toChecksumAddress(txResult.to);
//       let abi = require(`${__dirname}/abi/`+contractAddress+`.json`);
//
//       console.log(contractAddress);
//       console.log(txResult.input);
//       console.log("-------");
//
//       abiDecoder.addABI(abi);
//
//       let decoded = abiDecoder.decodeMethod(txResult.input);
//       console.log(decoded);
//
//     } catch (e) {
//       console.log(e);
//       console.log("Contract ABI not found, not converting input data.");
//     }
//   } else {
//     console.log(txResult.input);
//     console.log("do normal tx things");
//   }
//
// });
