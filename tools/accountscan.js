"use strict"

const Web3 = require("web3");
const InputDataDecoder = require('ethereum-input-data-decoder');
const abiDecoder = require('abi-decoder');
var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/";
var dbSettings = { useUnifiedTopology: true };
const settings = require("../cfgs/settings.json");
var web3 = new Web3(new Web3.providers.HttpProvider(settings.web3HTTP));

var speed = 50; // ms

function do_thing() {
  MongoClient.connect(url,dbSettings, function(err, db) {
    var dbo = db.db("egemEXP");
    if (err) throw err;

    dbo.collection("accounts").find().toArray(function(err, result) {
      if (err) throw err;

      function delay() {
        return new Promise(resolve => setTimeout(resolve, speed));
      }

      async function delayedLog(item) {
        // await promise return
        await delay();

        let addr_code = await web3.eth.getCode(item.address);
        //console.log(item.address+" | "+addr_code);
        if (addr_code.length > 2) {
          console.log("Contract found: "+item.address);

          let query = { address: item.address };
          var newvalues = { $set: {type: 1} };

          dbo.collection("accounts").updateOne(query, newvalues, function(err, res) {
            if (err) {

            } else {
              console.log("User updated");
              return;
            }
          });
        } else {
          let query = { address: item.address };
          var newvalues = { $set: {type: 0} };

          dbo.collection("accounts").updateOne(query, newvalues, function(err, res) {
            if (err) {
              console.log(err);
            } else {
              console.log("User updated");
              return;
            }
          });
        }
      }

      async function processArray(array) {
        for (const item of array) {
          await delayedLog(item);
        }
      }
      processArray(result);

    })
  })
}
 do_thing();
