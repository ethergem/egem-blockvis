"use strict"

const Web3 = require("web3");
const abiDecoder = require('abi-decoder');
var net = require('net');
var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/";
var dbSettings = { useUnifiedTopology: true };
const settings = require("../cfgs/settings.json");
var web3 = new Web3(new Web3.providers.HttpProvider(settings.web3HTTP));
//var web3 = new Web3(new Web3.providers.IpcProvider(settings.web3IPC, net));
var start = 0;

// time in ms
var fetchInterval= 50;

function timeConverter(UNIX_timestamp){
  var a = new Date(UNIX_timestamp * 1000);
  var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
  var year = a.getFullYear();
  var month = months[a.getMonth()];
  var date = a.getDate();
  var hour = a.getHours();
  var min = a.getMinutes();
  var sec = a.getSeconds();
  var time = date + ' ' + month + ' ' + year + ' ' + hour + ':' + min + ':' + sec ;
  return time;
}

function mainLoop(){
  // connection to mongodb
  MongoClient.connect(url,dbSettings, function(err, db) {
    var dbo = db.db("egemEXP");
    if (err) throw err;

    // process block
    async function processBlock(i) {
      await web3.eth.getBlock(i, function(error, result){

        let transactions = result.transactions;
        let uncles = result.uncles;
        let blockNumber = result.number;
        let miner = web3.utils.toChecksumAddress(result.miner);
        var firstseen = timeConverter(result.timestamp);

        var data = {
          blockNumber: result.number,
          hash: result.hash,
          parentHash: result.parentHash,
          sha3Uncles: result.sha3Uncles,
          transactionsRoot: result.transactionsRoot,
          stateRoot: result.stateRoot,
          miner: miner,
          nonce: result.nonce,
          timestamp: result.timestamp,
          timestampF: timeConverter(result.timestamp),
          difficulty: result.difficulty,
          totalDifficulty: result.totalDifficulty,
          size: result.size,
          extraData: web3.utils.hexToAscii(result.extraData),
          gasLimit: result.gasLimit,
          gasUsed: result.gasUsed,
          transactions: result.transactions,
          uncles: result.uncles
        };

        //console.log(data);
        insertBlock(data);
        insertUser(miner,0,firstseen);
        updateMined(miner);

        if (transactions.length > 0) {
          processTxs(transactions);
        }

        if (uncles.length > 0) {
          insertUncle(uncles,blockNumber);
        }

      })
    }

    // insert transactions
    function processTxs(transactions) {
      for (var i = 0; i < transactions.length; i++) {
        web3.eth.getTransaction(transactions[i], function(err, txResult){
          if (!err) {
            web3.eth.getBlock(txResult.blockNumber, function(err, txResult2){
              if (!err) {
                var firstseen = timeConverter(txResult2.timestamp);
                var fromUser = web3.utils.toChecksumAddress(txResult.from);
                if (txResult.input.length > 2  && !txResult.input.includes("0x456e6a6f7920746865204547454d2c2077697468206c6f76652066726f6d20746865204547454d20626f742e")) {
                  if (txResult.to == null) {

                    // contract creation
                    var data = {
                      hash: txResult.hash,
                      blockhash: txResult.blockHash,
                      blocknum: txResult.blockNumber,
                      timestamp: txResult2.timestamp,
                      timestampF: timeConverter(txResult2.timestamp),
                      nonce: txResult.nonce,
                      fromUser: fromUser,
                      toUser: txResult.to,
                      value: txResult.value,
                      gasPrice: txResult.gasPrice,
                      gas: txResult.gas,
                      txindex: txResult.transactionIndex,
                      inputData: txResult.input
                    };

                    getReceipt(txResult.hash,firstseen);
                    insertUser(fromUser,0,firstseen);
                    updateContractCount(fromUser);
                    updateSent(fromUser,firstseen);
                    insertTx(data);

                  } else {

                    // contract interaction
                    try {
                      let contractAddress = web3.utils.toChecksumAddress(txResult.to);
                      let abi = require(`../abi/`+contractAddress+`.json`);

                      console.log(contractAddress);
                      console.log(txResult.input);
                      console.log("-------");

                      abiDecoder.addABI(abi);

                      let decoded = abiDecoder.decodeMethod(txResult.input);
                      //console.log(decoded);

                      var data0x = {
                        hash: txResult.hash,
                        blockhash: txResult.blockHash,
                        blocknum: txResult.blockNumber,
                        timestamp: txResult2.timestamp,
                        timestampF: timeConverter(txResult2.timestamp),
                        nonce: txResult.nonce,
                        fromUser: fromUser,
                        toUser: contractAddress,
                        value: txResult.value,
                        gasPrice: txResult.gasPrice,
                        gas: txResult.gas,
                        txindex: txResult.transactionIndex,
                        inputData: decoded
                      };

                      insertUser(contractAddress,1,firstseen);
                      updateRecv(contractAddress,firstseen);
                      insertUser(fromUser,0,firstseen);
                      updateSent(fromUser,firstseen);
                      insertTx(data0x);
                      return;

                    } catch (e) {

                      // abi not found act as normal transaction.

                      if (txResult.to != null) {
                        var toUser = web3.utils.toChecksumAddress(txResult.to);
                        insertUser(toUser,0);
                        updateRecv(toUser,firstseen);
                      } else {
                        var toUser = "Contract Creation"
                      }

                      var data = {
                        hash: txResult.hash,
                        blockhash: txResult.blockHash,
                        blocknum: txResult.blockNumber,
                        timestamp: txResult2.timestamp,
                        timestampF: timeConverter(txResult2.timestamp),
                        nonce: txResult.nonce,
                        fromUser: fromUser,
                        toUser: toUser,
                        value: txResult.value,
                        gasPrice: txResult.gasPrice,
                        gas: txResult.gas,
                        txindex: txResult.transactionIndex,
                        inputData: txResult.input
                      };

                      insertUser(fromUser,0,firstseen);
                      updateSent(fromUser,firstseen);

                      insertTx(data);

                      //console.log(e);
                      console.log("Contract ABI not found, not converting input data.");
                      return;
                    }
                  }
                } else {

                  // normal transaction

                  if (txResult.to != null) {
                    var toUser = web3.utils.toChecksumAddress(txResult.to);
                    insertUser(toUser,0,firstseen);
                    updateRecv(toUser,firstseen);
                  } else {
                    var toUser = "Contract Creation"
                  }

                  var data = {
                    hash: txResult.hash,
                    blockhash: txResult.blockHash,
                    blocknum: txResult.blockNumber,
                    timestamp: txResult2.timestamp,
                    timestampF: timeConverter(txResult2.timestamp),
                    nonce: txResult.nonce,
                    fromUser: fromUser,
                    toUser: toUser,
                    value: txResult.value,
                    gasPrice: txResult.gasPrice,
                    gas: txResult.gas,
                    txindex: txResult.transactionIndex,
                    inputData: txResult.input
                  };

                  insertUser(fromUser,0,firstseen);
                  updateSent(fromUser,firstseen);

                  insertTx(data);

                  return;
                }
              }
            })

          }

        })
      }
    }

    // insert transaction
    function insertTx(data){
      //console.log(data);
      dbo.collection("transactions").insertOne(data, function(err, res) {
        if (err) throw err;
        //console.log("1 transaction inserted");
      });
    }

    // insert transaction
    function getReceipt(hash,firstseen){
      web3.eth.getTransactionReceipt(hash, function(err, txResult){
        if (!err) {
          let createdAddress = txResult.contractAddress;
          // insert user
          if (createdAddress) {
            insertUser(createdAddress,1,firstseen);
          }
        }
      })
    }

    // insert user
    function insertUser(i,type,firstseen) {
      var query = { address: i }
      let data = {
        address: i,
        type: type,
        contracts: 0,
        sent: 0,
        recv: 0,
        minedblocks: 0,
        mineduncles: 0,
        bal: 0,
        firstseen: firstseen
      }
      dbo.collection("accounts").insertOne(data, function(err, res) {
        //if (err) console.log("Address Skipped.");
        //console.log("1 user inserted");
      });
    }

    // insert block
    function insertBlock(i) {
      dbo.collection("blocks").insertOne(i, function(err, res) {
        if (err) throw err;
        //console.log("Block inserted");
      });
    }

    // insert uncle
    function insertUncle(uncles,blockNumber) {
      for (var i = 0; i < uncles.length; i++) {
        web3.eth.getUncle(blockNumber,i, function(err, result){
          let miner = web3.utils.toChecksumAddress(result.miner);
          var data = {
            blockNumber: result.number,
            hash: result.hash,
            miner: miner,
            nonce: result.nonce,
            extraData: web3.utils.hexToAscii(result.extraData),
            difficulty: result.difficulty,
            gasLimit: result.gasLimit,
            gasUsed: result.gasUsed,
            timestamp: result.timestamp,
            timestampF: timeConverter(result.timestamp),
            uncles: result.uncles
          };
          updateMinedUncle(miner);
          dbo.collection("uncles").insertOne(data, function(err, res) {
            if (err) throw err;
            //console.log("1 unlce inserted");
          });
        })
      }
    }

    // update sent count
    function updateSent(i,firstseen) {
      let query = { address: i };
      var newvalues = { $inc: {sent: 1} };
      dbo.collection("accounts").updateOne(query, newvalues, function(err, res) {
        if (err) {
          console.log(err);
          insertUser(i,0,firstseen);
          updateSent(i,firstseen);
        } else {
          //console.log("User sent updated");
          return;
        }
      });
    }

    // update recv count
    function updateRecv(i,firstseen) {
      let query = { address: i };
      var newvalues = { $inc: {recv: 1} };
      dbo.collection("accounts").updateOne(query, newvalues, function(err, res) {
        if (err) {
          console.log(err);
          insertUser(i,0,firstseen);
          updateRecv(i,firstseen);
        } else {
          //console.log("User recv updated");
          return;
        }
      });
    }

    // update mined count
    function updateMined(i) {
      let query = { address: i };
      var newvalues = { $inc: {minedblocks: 1} };
      dbo.collection("accounts").updateOne(query, newvalues, function(err, res) {
        if (err) {
          console.log(err);
          insertUser(i,0,firstseen);
          updateMined(i);
        } else {
          //console.log("User mined updated");
          return;
        }
      });
    }

    // update uncle count
    function updateMinedUncle(i) {
      let query = { address: i };
      var newvalues = { $inc: {mineduncles: 1} };
      dbo.collection("accounts").updateOne(query, newvalues, function(err, res) {
        if (err) {
          console.log(err);
          insertUser(i,0,firstseen);
          updateMinedUncle(i);
        } else {
          //console.log("User uncle updated");
          return;
        }
      });
    }

    // contracts count
    function updateContractCount(i) {
      let query = { address: i };
      var newvalues = { $inc: {contracts: 1} };
      dbo.collection("accounts").updateOne(query, newvalues, function(err, res) {
        if (err) {
          console.log(err);
        } else {
          //console.log("User uncle updated");
          return;
        }
      });
    }

    // get last stored block number
    var getLast = new Promise(function(resolve, reject) {
      dbo.collection("blocks").countDocuments(function(err, res) {
        if (err) throw err;
        //console.log(res);
        resolve(res);
      })
    });

    // Sync Loop
    function sync() {
      console.log("----SYNCING STARTING----");

      getLast.then(res => {
        let start = res;
        setInterval(async function() {
          var currentBlock = Promise.resolve(web3.eth.getBlockNumber());
          var handleBlocks = currentBlock.then(value => {
              console.log("Current block: " + value);
              console.log(start);
              if (start < value) {
                start++
                console.log("----- Syncing Block: ");
                processBlock(start);
              } else {
                console.log("----- We are synced...");
              }
          });
        }, fetchInterval)
      })
    }
    // starts the sync loop/interval
    sync();
  })
}

// starts everything
mainLoop();
