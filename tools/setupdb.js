"use strict"

const Web3 = require("web3");
var net = require('net');
var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/";
var dbSettings = { useUnifiedTopology: true };
const settings = require("../cfgs/settings.json");
//var web3 = new Web3(new Web3.providers.IpcProvider(settings.web3IPC, net));
var web3 = new Web3(new Web3.providers.HttpProvider(settings.web3HTTP));
MongoClient.connect(url,dbSettings, function(err, db) {
  if (err) throw err;

  // set database
  var dbo = db.db("egemEXP");

  // set collections
  // dbo.createCollection("accounts", function(err, res) {
  //   if (err) throw err;
  //   console.log("Accounts Collection created!");
  // });
  // dbo.createCollection("blocks", function(err, res) {
  //   if (err) throw err;
  //   console.log("Blocks Collection created!");
  // });
  // dbo.createCollection("transactions ", function(err, res) {
  //   if (err) throw err;
  //   console.log("Transactions Collection created!");
  // });
  // dbo.createCollection("uncles", function(err, res) {
  //   if (err) throw err;
  //   console.log("Uncles Collection created!");
  // });

  var infoData = {
    accounts: 0,
    transactions: 0,
    contracts: 0,
    blocks: 0,
    uncles: 0,
    supply: 0,
    price: 0
  }

  dbo.createCollection("information", function(err, res) {
    if (err) throw err;
    console.log("Information Collection created!");
    dbo.collection("information").insertOne(infoData, function(err, res) {
      if (err) throw err;
      console.log("Info inserted");
    });
  });

  // set indexes
  dbo.collection("accounts").createIndex({ address: 1 },{unique:true}, function(err, res) {
    if (err) throw err;
    console.log("index created");
  });
  dbo.collection("blocks").createIndex({ blockNumber: 1 },{unique:true}, function(err, res) {
    if (err) throw err;
    console.log("index created");
  });
  dbo.collection("blocks").createIndex({ miner: 1 }, function(err, res) {
    if (err) throw err;
    console.log("index created");
  });
  dbo.collection("transactions").createIndex({ hash: 1 }, function(err, res) {
    if (err) throw err;
    console.log("index created");
  });
  dbo.collection("transactions").createIndex({ fromUser: 1 }, function(err, res) {
    if (err) throw err;
    console.log("index created");
  });
  dbo.collection("transactions").createIndex({ toUser: 1 }, function(err, res) {
    if (err) throw err;
    console.log("index created");
  });
  dbo.collection("transactions").createIndex({ blocknum: -1 }, function(err, res) {
    if (err) throw err;
    console.log("index created");
  });
  dbo.collection("uncles").createIndex({ blockNumber: 1 }, function(err, res) {
    if (err) throw err;
    console.log("index created");
  });
  dbo.collection("uncles").createIndex({ miner: 1 }, function(err, res) {
    if (err) throw err;
    console.log("index created");
  });

});
