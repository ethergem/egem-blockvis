"use strict"

const Web3 = require("web3");
const express = require('express');
const session = require('express-session');
const bodyParser = require('body-parser');
var Promise = require('bluebird');
const settings = require("./cfgs/settings.json");
const app = express();
const net = require('net');
const cluster = require('cluster');
//var web3 = new Web3(new Web3.providers.IpcProvider(settings.web3IPC, net));
var web3 = new Web3(new Web3.providers.HttpProvider(settings.web3HTTP));
const helmet = require('helmet');
const tagline = "egem.io"
const rateLimit = require("express-rate-limit");

var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/";
var dbSettings = { useUnifiedTopology: true };

app.use(session({
	secret: settings.sessionsecret,
	resave: true,
	saveUninitialized: true
}));

app.use(bodyParser.urlencoded({extended : true}));
app.use(bodyParser.json());
app.use(helmet());
app.set('view engine', 'pug');
app.use(express.static(__dirname + '/public/'));
app.set('trust proxy', true);

var expressDefend = require('express-defend');
var blacklist = require('express-blacklist');

app.use(blacklist.blockRequests('blacklist.txt'));
app.use(expressDefend.protect({
    maxAttempts: 3,
    dropSuspiciousRequest: true,
    logFile: 'suspicious.log',
    onMaxAttemptsReached: function(ipAddress, url){
        blacklist.addAddress(ipAddress);
    }
}));

const limiter = rateLimit({
  windowMs: 5 * 60 * 1000, // 1 minutes
  max: 25, // limit each IP to 25 requests per windowMs
	message: "This explorer is not intended for this type of action so please stop, you have been rate limited.",
	statusCode: 403
});

//  apply to all requests
app.use(limiter);

// Home redirect
app.get('/home', function(req, res) {
	res.redirect('/');
});

// Landing page
app.get('/', (req, resp) => {
	var blockLimit = 3;
	var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
	console.log("Site opened from: "+ip);
	async function getMain() {
		try {
			MongoClient.connect(url,dbSettings, function(err, db) {
				var dbo = db.db("egemEXP");
				if (err) console.log(err);
				dbo.collection("information").find().toArray(function(err, result1) {
					if (err) console.log(err);
					dbo.collection("blocks").find().sort( {blockNumber: -1} ).limit(blockLimit).toArray(function(err, result2) {
						if (err) console.log(err);
						dbo.collection("transactions").find().sort( {blocknum: -1} ).limit(blockLimit).toArray(function(err, result3) {
							if (err) console.log(err);
							var currentBlock = Promise.resolve(web3.eth.getBlockNumber());
							var chainInfo = result1;
							var priceInfo = result1[0]['price'];
							var lastBlocks = result2;
							var lastTxs = result3;
							currentBlock.then( value => {
								var chainHead = value;
								let date_ob = new Date();
								resp.render('content/index', {
									title: 'Home',
									tagline,
									chainHead,
									date_ob,
									chainInfo,
									priceInfo,
									lastBlocks,
									lastTxs
								});
							})
							db.close();
						})
					})

				})
			})
		} catch (e) {
			console.log(e);
		}
	}
	getMain();
});

// Richlist
app.get('/richlist/limit/:limit/page/:page', (req, resp) => {
	var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
	console.log("Richlist opened from: "+ip);

	let limitPer = req.params.limit;
	let page = req.params.page;

	if (limitPer <= 0 || limitPer > 100 || isNaN(limitPer)) {
		limitPer = "50";
	}
	if (isNaN(page) || page <= 0) {
		page = "1";
	}
	// calculate offset
	const offset = (page - 1) * limitPer

	var limit = 50;

	async function getRich() {
		try {
			MongoClient.connect(url,dbSettings, function(err, db) {
				var dbo = db.db("egemEXP");
				if (err) console.log(err);
				dbo.collection("accounts").find({ bal: { $gt: 10000 } }).skip(Number(offset)).limit(Number(limitPer)).sort({ bal: -1}).toArray(function(err, result1) {
					if (err) console.log(err);
					dbo.collection("information").find().toArray(function(err, result2) {
						if (err) console.log(err);
						//console.log(result);
							var richlist = result1;
							var priceInfo = result2[0]['price'];
							var supInfo = result2[0]['supply'];
							resp.render('content/richlist', {
								title: 'Richlist',
								tagline,
								richlist,
								priceInfo,
								supInfo,
								page,
								offset
							});
						db.close();
					})
				})
			})
		} catch (e) {
			console.log(e);
		}
	}
	getRich();
});

// Contracts
app.get('/contracts/limit/:limit/page/:page', (req, resp) => {
	var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
	console.log("Contracts opened from: "+ip);

	let limitPer = req.params.limit;
	let page = req.params.page;

	if (limitPer <= 0 || limitPer > 100 || isNaN(limitPer)) {
		limitPer = "50";
	}
	if (isNaN(page) || page <= 0) {
		page = "1";
	}
	// calculate offset
	const offset = (page - 1) * limitPer

	var limit = 50;

	async function getContracts() {
		try {
			MongoClient.connect(url,dbSettings, function(err, db) {
				var dbo = db.db("egemEXP");
				if (err) console.log(err);
				dbo.collection("accounts").find({ type: { $gt: 0 } }).skip(Number(offset)).limit(Number(limitPer)).sort({ bal: -1}).toArray(function(err, result1) {
					if (err) console.log(err);
					dbo.collection("information").find().toArray(function(err, result2) {
						if (err) console.log(err);
						//console.log(result);
							var richlist = result1;
							var priceInfo = result2[0]['price'];
							var supInfo = result2[0]['supply'];
							resp.render('content/contracts', {
								title: 'Contracts',
								tagline,
								richlist,
								priceInfo,
								supInfo,
								page,
								offset
							});
						db.close();
					})
				})
			})
		} catch (e) {
			console.log(e);
		}
	}
	getContracts();
});

// Block list
app.get('/blocks/limit/:limit/page/:page', (req, resp) => {
	var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
	console.log("Blocks opened from: "+ip);

	let limitPer = req.params.limit;
	let page = req.params.page;

	if (limitPer <= 0 || limitPer > 100 || isNaN(limitPer)) {
		limitPer = "50";
	}
	if (isNaN(page) || page <= 0) {
		page = "1";
	}
	// calculate offset
	const offset = (page - 1) * limitPer

	var limit = 50;

	async function getBlockList() {
		try {
			MongoClient.connect(url,dbSettings, function(err, db) {
				var dbo = db.db("egemEXP");
				if (err) console.log(err);
				dbo.collection("blocks").find({ blockNumber: { $gt: 0 } }).skip(Number(offset)).limit(Number(limitPer)).sort({ blockNumber: -1}).toArray(function(err, result1) {
					if (err) console.log(err);
					dbo.collection("information").find().toArray(function(err, result2) {
						if (err) console.log(err);
						//console.log(result);
							var blocklist = result1;
							var priceInfo = result2[0]['price'];
							var supInfo = result2[0]['supply'];
							resp.render('content/blocks', {
								title: 'Blocks',
								tagline,
								blocklist,
								priceInfo,
								supInfo,
								page,
								offset
							});
						db.close();
					})
				})
			})
		} catch (e) {
			console.log(e);
		}
	}
	getBlockList();
});

// Uncles
app.get('/uncles/limit/:limit/page/:page', (req, resp) => {
	var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
	console.log("Uncles opened from: "+ip);

	let limitPer = req.params.limit;
	let page = req.params.page;

	if (limitPer <= 0 || limitPer > 100 || isNaN(limitPer)) {
		limitPer = "50";
	}
	if (isNaN(page) || page <= 0) {
		page = "1";
	}
	// calculate offset
	const offset = (page - 1) * limitPer

	var limit = 50;

	async function getUncleList() {
		try {
			MongoClient.connect(url,dbSettings, function(err, db) {
				var dbo = db.db("egemEXP");
				if (err) console.log(err);
				dbo.collection("uncles").find({ blockNumber: { $gt: 0 } }).skip(Number(offset)).limit(Number(limitPer)).sort({ blockNumber: -1}).toArray(function(err, result1) {
					if (err) console.log(err);
					dbo.collection("information").find().toArray(function(err, result2) {
						if (err) console.log(err);
						//console.log(result);
							var unclelist = result1;
							var priceInfo = result2[0]['price'];
							var supInfo = result2[0]['supply'];
							resp.render('content/uncles', {
								title: 'Uncles',
								tagline,
								unclelist,
								priceInfo,
								supInfo,
								page,
								offset
							});
						db.close();
					})
				})
			})
		} catch (e) {
			console.log(e);
		}
	}
	getUncleList();
});

// Faq
app.get('/faq', (req, resp) => {
	var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
	console.log("Faq opened from: "+ip);

	async function getFaq() {
		try {
			resp.render('content/faq', {
				title: 'F.a.q',
				tagline
			});
		} catch (e) {
			console.log(e);
		}
	}
	getFaq();
});

// https://explorer.egem.io/addr/
app.get('/addr/:address/txs/:txs/limit/:limit/page/:page', (req, resp) => {
	var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
	let direction = req.params.txs;
	let limitPer = req.params.limit;
	let page = req.params.page;

	console.log(req.params.address+" Address & txs opened from: "+ip+" Page/limit/Dir: "+page+"/"+limitPer+"/"+direction);

	if (limitPer <= 0 || limitPer > 100 || isNaN(limitPer)) {
		limitPer = "100";
	}
	if (isNaN(page) || page <= 0) {
		page = "1";
	}
	// calculate offset
	const offset = (page - 1) * limitPer

	async function getAddr() {
		try {
			MongoClient.connect(url,dbSettings, function(err, db) {
				var dbo = db.db("egemEXP");
				if (err) console.log(err);
				let user_addr = web3.utils.toChecksumAddress(req.params.address);
				var resultAddress = user_addr.match(/^0x[a-fA-F0-9]{40}$/);
				dbo.collection("information").find().toArray(function(err, result2) {
					if (err) console.log(err);
					var priceInfo = result2[0]['price'];
					if (resultAddress != null) {
						if (direction === "out") {
							dbo.collection("transactions").find({ "fromUser": user_addr }).skip(Number(offset)).limit(Number(limitPer)).sort({ blocknum: -1}).toArray(function(err, result) {
								if (err) console.log(err);
								//console.log(result);
									let txListOut = result;
									//console.log(block)
									resp.render('content/accountTxOut', {
										title: 'Account & Transactions Inward',
										tagline,
										txListOut,
										user_addr,
										priceInfo,
										page
									});
								db.close();
							})
						} else if (direction === "in") {
							dbo.collection("transactions").find({ "toUser": user_addr }).skip(Number(offset)).limit(Number(limitPer)).sort({ blocknum: -1}).toArray(function(err, result) {
								if (err) console.log(err);
								//console.log(result);
									let txListIn = result;
									//console.log(block)
									resp.render('content/accountTxIn', {
										title: 'Account & Transactions Outward',
										tagline,
										txListIn,
										user_addr,
										priceInfo,
										page
									});
								db.close();
							})
						} else if (direction === "contracts") {
							dbo.collection("transactions").find({ "fromUser": user_addr, "toUser": null }).skip(Number(offset)).limit(Number(limitPer)).sort({ blocknum: -1}).toArray(function(err, result) {
								if (err) console.log(err);
								//console.log(result);
									let contractList = result;
									//console.log(block)
									resp.render('content/accountTxContracts', {
										title: 'Contracts Created',
										tagline,
										contractList,
										user_addr,
										priceInfo,
										page
									});
								db.close();
							})
						} else {
							resp.status(200).send({error: "bad address lookup"})
						}
					} else {
						resp.status(200).send({error: "bad address lookup"})
					}
				})

			})
		} catch (e) {
			console.log(e);
		}
	}
	getAddr();
});

// https://explorer.egem.io/addr/blocks/
app.get('/addr/:address/blocks/:type/limit/:limit/page/:page', (req, resp) => {
	var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
	let type = req.params.type;
	let limitPer = req.params.limit;
	let page = req.params.page;

	console.log(req.params.address+" Blocks & Unlces opened from: "+ip+" Page/limit/Dir: "+page+"/"+limitPer+"/"+type);

	if (limitPer <= 0 || limitPer > 100 || isNaN(limitPer)) {
		limitPer = "100";
	}
	if (isNaN(page) || page <= 0) {
		page = "1";
	}
	// calculate offset
	const offset = (page - 1) * limitPer

	async function getAddr() {
		try {
			MongoClient.connect(url,dbSettings, function(err, db) {
				var dbo = db.db("egemEXP");
				if (err) console.log(err);
				let user_addr = web3.utils.toChecksumAddress(req.params.address);
				var resultAddress = user_addr.match(/^0x[a-fA-F0-9]{40}$/);
				dbo.collection("information").find().toArray(function(err, result2) {
					if (err) console.log(err);
					var priceInfo = result2[0]['price'];
					if (resultAddress != null) {
						if (type === "uncles") {
							dbo.collection("uncles").find({ "miner": user_addr }).skip(Number(offset)).limit(Number(limitPer)).sort({ blockNumber: -1}).toArray(function(err, result) {
								if (err) console.log(err);
								//console.log(result);
									let uncles = result;
									//console.log(block)
									resp.render('content/accountMinedUncles', {
										title: 'Uncles Mined',
										tagline,
										uncles,
										user_addr,
										priceInfo,
										page
									});
								db.close();
							})
						} else if (type === "mined") {
							dbo.collection("blocks").find({ "miner": user_addr }).skip(Number(offset)).limit(Number(limitPer)).sort({ blockNumber: -1}).toArray(function(err, result) {
								if (err) console.log(err);
								//console.log(result);
									let blocks = result;
									//console.log(block)
									resp.render('content/accountMinedBlocks', {
										title: 'Blocks Mined',
										tagline,
										blocks,
										user_addr,
										priceInfo,
										page
									});
								db.close();
							})
						} else {
							resp.status(200).send({error: "bad uncle lookup"})
						}
					} else {
						resp.status(200).send({error: "bad address lookup"})
					}
				})

			})
		} catch (e) {
			console.log(e);
		}
	}
	getAddr();
});

app.get('/addr/:address', (req, resp) => {
	var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
	console.log(req.params.address+" Address opened from: "+ip);
	async function getAddr() {
		var user_addr = web3.utils.toChecksumAddress(req.params.address);
		if (user_addr) {
			var resultAddress = user_addr.match(/^0x[a-fA-F0-9]{40}$/);
			if (resultAddress != null) {
				var address_balance = await web3.eth.getBalance(user_addr)
				var addr_code = await web3.eth.getCode(user_addr)
				let query = { address: user_addr };
				//console.log(query);
				try {
					MongoClient.connect(url,dbSettings, function(err, db) {
						var dbo = db.db("egemEXP");
						if (err) console.log(err);
						dbo.collection("accounts").find(query).toArray(function(err, res) {
							if (err) console.log(err);
							dbo.collection("information").find().toArray(function(err, result2) {
								if (err) console.log(err);
									var user_data = res;
									var priceInfo = result2[0]['price'];
									resp.status(200).render('content/account', {
										title: 'Account & Transactions',
										tagline,
										addr_bal: address_balance,
										user_addr,
										user_data,
										addr_code,
										priceInfo
									});
									db.close();
							})
						});
					});
				} catch (e) {
					console.log(e);
				}
			} else {
				resp.status(200).send({error: "bad address lookup"})
			}
		} else {
			resp.status(200).render('content/accountBlank', {
				title: 'Account & Transactions',
				tagline
			});
		}

	}
	getAddr();
});

// Address Variants

app.get('/address/:address', function(req, res){
	res.redirect('/addr/'+req.params.address);
});

// Transactions

app.get('/tx/:hash', (req, resp) => {
	var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
	async function getTx() {
		var user_hash = req.params.hash;
		console.log("Txs opened from: "+ip+" Hash: "+user_hash);
		if (user_hash) {
			var resultTx = user_hash.match(/^0x[a-fA-F0-9]{64}$/)
			var tx_reciept = await web3.eth.getTransactionReceipt(user_hash)
			console.log(tx_reciept.contractAddress);
				if (resultTx != null) {
					try {
						MongoClient.connect(url,dbSettings, function(err, db) {
							var dbo = db.db("egemEXP");
							if (err) console.log(err);
							dbo.collection("transactions").find({ "hash": user_hash }).toArray(function(err, result1) {
								if (err) console.log(err);
								if (result1.length > 0) {
									dbo.collection("information").find().toArray(function(err, result2) {
										if (err) console.log(err);
										let txResult = result1;
										var priceInfo = result2[0]['price'];

										let txData = [{
											hash: txResult[0]['hash'],
											blockhash: txResult[0]['blockhash'],
											blocknum: txResult[0]['blocknum'],
											timestamp: txResult[0]['timestamp'],
											timestampF: txResult[0]["timestampF"],
											nonce:  txResult[0]['nonce'],
											fromUser: txResult[0]['fromUser'],
											toUser: txResult[0]['toUser'],
											value: txResult[0]['value'],
											gasPrice: txResult[0]['gasPrice'],
											gas: txResult[0]['gas'],
											inputData0: JSON.stringify(txResult[0]['inputData']),
											inputData1: JSON.stringify(txResult[0]['inputData']['name']),
											inputData2: JSON.stringify(txResult[0]['inputData']['params'])
										}]
										//console.log(txResult[0]['inputData']);
										console.log(txResult[0]['inputData']['params']);
										resp.status(200).render('content/tx', {
											title: 'Transaction Result',
											tagline,
											txData,
											priceInfo,
											tx_reciept
										});
										db.close();
									})
								} else {
									resp.status(200).send({error: "bad tx lookup, or tx not found."})
								}

							})
						})
					} catch (e) {
						console.log(e);
					}
				} else {
					resp.status(200).send({error: "bad tx lookup"})
				}
		} else {
			resp.status(200).render('content/txBlank', {
				title: 'Transaction Result',
				tagline
			});
		}

	}
	getTx();
});

app.get('/uncle/:hash', (req, resp) => {
	var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
	console.log("Uncles opened from: "+ip);
	async function getUncle() {
		var user_hash = req.params.hash;
		if (user_hash) {
			var resultTx = user_hash.match(/^0x[a-fA-F0-9]{64}$/)
				if (resultTx != null) {
					try {
						MongoClient.connect(url,dbSettings, function(err, db) {
							var dbo = db.db("egemEXP");
							if (err) console.log(err);
							dbo.collection("uncles").find({ "hash": user_hash }).toArray(function(err, result1) {
								if (err) console.log(err);
								//console.log(result);
									let uncleResult = result1;
									//console.log(block)
									resp.status(200).render('content/uncle', {
										title: 'Uncle Result',
										tagline,
										uncleResult
									});
								db.close();
							})
						})
					} catch (e) {
						console.log(e);
					}
				} else {
					resp.status(200).send({error: "bad uncle lookup"})
				}
		} else {
			resp.status(200).render('content/unclesBlank', {
				title: 'Uncle Result',
				tagline
			});
		}

	}
	getUncle();
});

app.get('/block/:number', (req, resp) => {
	var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
	async function getBlock() {
		var user_number = req.params.number;
		if (user_number) {
			var resultBlock = user_number.match(/^[0-9][0-9]*$/)
				if (resultBlock != null) {
					try {
						console.log("Block "+user_number+" opened from: "+ip);
						MongoClient.connect(url,dbSettings, function(err, db) {
							var dbo = db.db("egemEXP");
							if (err) console.log(err);
							dbo.collection("blocks").find({ "blockNumber": Number(user_number) }).toArray(function(err, result) {
								if (err) console.log(err);
								if (result.length > 0) {

									let txlist = result[0]["transactions"];
									let unclelist = result[0]["uncles"];

									let blockResult = [{
										hash: result[0]["hash"],
										parentHash: result[0]["parentHash"],
										blockNumber: result[0]["blockNumber"],
										sha3Uncles: result[0]["sha3Uncles"],
										transactionsRoot: result[0]["transactionsRoot"],
										stateRoot: result[0]["stateRoot"],
										miner: result[0]["miner"],
										nonce: result[0]["nonce"],
										size: result[0]["size"],
										timestamp: result[0]["timestamp"],
										timestampF: result[0]["timestampF"],
										totalDifficulty: result[0]["totalDifficulty"],
										difficulty: result[0]["difficulty"],
										extraData: result[0]["extraData"],
										gasLimit: result[0]["gasLimit"],
										gasUsed: result[0]["gasUsed"],
										transactions: txlist,
										uncles: unclelist
									}];

									resp.status(200).render('content/block', {
										title: 'Block Result',
										tagline,
										blockResult,
										txlist,
										unclelist
									});

								db.close();
								} else {
									resp.status(200).render('content/blockBlank', {
										title: 'Block Result',
										tagline
									});
								}
							})
						})
					} catch (e) {
						console.log(e);
					}
				} else {
					resp.status(200).send({error: "bad tx lookup"})
				}
		} else {
			resp.status(200).render('content/blockBlank', {
				title: 'Block Result',
				tagline
			});
		}

	}
	getBlock();
});

// Explorer API

app.get('/api/supply', (req, res) => {
	try {
		MongoClient.connect(url,dbSettings, function(err, db) {
			var dbo = db.db("egemEXP");
			if (err) console.log(err);
			dbo.collection("information").find().toArray(function(err, result) {
				if (err) console.log(err);
				let supply = result[0]['supply'];
				res.status(200).send({estimated_supply: supply})
			})
		})
	} catch (e) {
		console.log(e);
	}
});

app.get('/api/stats', (req, res) => {
	try {
		MongoClient.connect(url,dbSettings, function(err, db) {
			var dbo = db.db("egemEXP");
			if (err) console.log(err);
			dbo.collection("information").find().toArray(function(err, result) {
				if (err) console.log(err);
				res.status(200).send({
					estimated_supply: result[0]['supply'],
					total_accounts: result[0]['accounts'],
					total_contracts: result[0]['contracts'],
					total_txs: result[0]['transactions'],
					total_blocks: result[0]['blocks'],
					total_uncles: result[0]['uncles']
				})
			})
		})
	} catch (e) {
		console.log(e);
	}
});

// Extra Functions

function timeConverter(UNIX_timestamp){
	var a = new Date(UNIX_timestamp * 1000);
	var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
	var year = a.getFullYear();
	var month = months[a.getMonth()];
	var date = a.getDate();
	var hour = a.getHours();
	var min = a.getMinutes();
	var sec = a.getSeconds();
	var time = date + ' ' + month + ' ' + year + ' ' + hour + ':' + min + ':' + sec ;
	return time;
}

const server = app.listen(3066, () => {
	console.log(`Blockvis → PORT: ${server.address().port} → WORKER: ${process.pid}`);
});
