function searchEnter() {
  // Get the input field
  var input = document.getElementById("searchEnter");

  // Execute a function when the user releases a key on the keyboard
  input.addEventListener("keyup", function(event) {
    // Number 13 is the "Enter" key on the keyboard
    if (event.keyCode === 13) {
      // Cancel the default action, if needed
      event.preventDefault();
      // Trigger the button element with a click
      document.getElementById("searchIt").click();
    }
  });
}

function searchBar() {
  //var checkBox = document.getElementById("nodeCheck");
  var str = document.querySelector('[name="mainInput"]').value;
  // regex
  var resultAddress = str.match(/^0x[a-fA-F0-9]{40}$/)
  var resultTx = str.match(/^0x[a-fA-F0-9]{64}$/)
  var resultBlock = str.match(/^[0-9][0-9]*$/)

  if (resultAddress != null) {
    window.location.replace('/addr/'+str);
  }
  if (resultTx != null) {
    window.location.replace('/tx/'+str);
  }
  if (resultBlock != null) {
    window.location.replace('/block/'+str);
  }

  // if (str == "latest") {
  //   window.location.replace('/blocks?=latest');
  // }
  if (str == "bot") {
    window.location.replace('/account/0xe3696bd9aeec0229bb9c98c9cc7220ce37852887');
  }

}

// this way sucks
function searchTxsIn() {
  var str1 = document.querySelector('[name="rangeInput"]').value;
  var str2 = document.querySelector('[name="pageInput"]').value;
  if (str1 > 1000 || isNaN(str1) || !str1) {
    var str1 = "10";
  }
  if (!str2 || isNaN(str2)) {
    var str2 = "1";
  }
  var addressBar = window.location.href
  var addressSend = addressBar.substring(addressBar.indexOf("addr/") + 5);

  if (str1 != null) {
    window.location.replace('/addr/'+addressSend+'/txs/in'+'/limit/'+str1+'/page/'+str2);
  }
}

function searchTxsOut() {
  var str1 = document.querySelector('[name="rangeInput"]').value;
  var str2 = document.querySelector('[name="pageInput"]').value;
  if (str1 > 1000 || isNaN(str1) || !str1) {
    var str1 = "10";
  }
  if (!str2 || isNaN(str2)) {
    var str2 = "1";
  }
  var addressBar = window.location.href
  var addressSend = addressBar.substring(addressBar.indexOf("addr/") + 5);

  if (str1 != null) {
    window.location.replace('/addr/'+addressSend+'/txs/out'+'/limit/'+str1+'/page/'+str2);
  }
}

function searchContracts() {
  var str1 = document.querySelector('[name="rangeInput"]').value;
  var str2 = document.querySelector('[name="pageInput"]').value;
  if (str1 > 1000 || isNaN(str1) || !str1) {
    var str1 = "10";
  }
  if (!str2 || isNaN(str2)) {
    var str2 = "1";
  }
  var addressBar = window.location.href
  var addressSend = addressBar.substring(addressBar.indexOf("addr/") + 5);

  if (str1 != null) {
    window.location.replace('/addr/'+addressSend+'/txs/contracts'+'/limit/'+str1+'/page/'+str2);
  }
}

function searchMinedBlocks() {
  var str1 = document.querySelector('[name="rangeInput"]').value;
  var str2 = document.querySelector('[name="pageInput"]').value;
  if (str1 > 1000 || isNaN(str1) || !str1) {
    var str1 = "10";
  }
  if (!str2 || isNaN(str2)) {
    var str2 = "1";
  }
  var addressBar = window.location.href
  var addressSend = addressBar.substring(addressBar.indexOf("addr/") + 5);

  if (str1 != null) {
    window.location.replace('/addr/'+addressSend+'/blocks/mined'+'/limit/'+str1+'/page/'+str2);
  }
}

function searchMinedUncles() {
  var str1 = document.querySelector('[name="rangeInput"]').value;
  var str2 = document.querySelector('[name="pageInput"]').value;
  if (str1 > 1000 || isNaN(str1) || !str1) {
    var str1 = "10";
  }
  if (!str2 || isNaN(str2)) {
    var str2 = "1";
  }
  var addressBar = window.location.href
  var addressSend = addressBar.substring(addressBar.indexOf("addr/") + 5);

  if (str1 != null) {
    window.location.replace('/addr/'+addressSend+'/blocks/uncles'+'/limit/'+str1+'/page/'+str2);
  }
}

function nextPage() {
  var x = document.getElementById("pageNum").innerText;
  var nextPage = (Number(x) + 1);
  if (nextPage <= 0 || isNaN(nextPage)) {
    window.location.replace("1");
  } else {
    window.location.replace(nextPage);
  }
}

function prevPage() {
  var x = document.getElementById("pageNum").innerText;
  var prevPage = (Number(x) - 1);
  if (prevPage <= 0 || isNaN(prevPage)) {
    window.location.replace("1");
  } else {
    window.location.replace(prevPage);
  }
}

function nextBlock() {
  var x = document.getElementById("blockNum").innerText;
  var nextBlock = (Number(x) + 1);
  if (nextBlock <= 0 || isNaN(nextBlock)) {
    window.location.replace("1");
  } else {
    window.location.replace(nextBlock);
  }
}

function prevBlock() {
  var x = document.getElementById("blockNum").innerText;
  var prevBlock = (Number(x) - 1);
  if (prevBlock <= 0 || isNaN(prevBlock)) {
    window.location.replace("1");
  } else {
    window.location.replace(prevBlock);
  }
}
